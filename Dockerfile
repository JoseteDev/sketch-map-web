### STAGE 1: Build ###
FROM node:14.11.0-alpine AS build-stage
ARG ENVIRONMENT
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN if [ "$ENVIRONMENT" = "prod" ] ; then npm run build-prod ; else npm run build ; fi

### STAGE 2: Run ###
FROM nginx:1.19.2-alpine
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /usr/src/app/dist/sketch-map /usr/share/nginx/html
