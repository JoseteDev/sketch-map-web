import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {IUser} from '../../interfaces/i-user';
import {UserService} from '../../services/user.service';
import {AuthService} from '../../services/auth.service';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  user: IUser;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private notifierService: NotifierService
  ) {}


  ngOnInit() {
    this.subscriptions.push(this.userService.user.subscribe(user => this.user = user));
  }


  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  doLogout() {
    this.authService.doLogout();
  }


  dev() {
    this.notifierService.notify('default', 'Currently in development...');
  }

}
