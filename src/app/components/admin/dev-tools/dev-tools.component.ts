import {Component} from '@angular/core';
import {NotifierService} from 'angular-notifier';
import {MatDialog} from '@angular/material/dialog';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';
import {UserDialogComponent} from '../../dialogs/user-dialog/user-dialog.component';

@Component({
  selector: 'app-dev-tools',
  templateUrl: './dev-tools.component.html',
  styleUrls: ['./dev-tools.component.scss']
})
export class DevToolsComponent {

  showSpinner: boolean;
  showBar: boolean;

  constructor(
    private notifierService: NotifierService,
    private matDialog: MatDialog
  ) {
  }


  showNotification(type: string, message: string) {
    this.notifierService.notify(type, message);
  }


  showDialog(type: string) {
    switch (type) {
      case 'confirmation':
        this.matDialog.open(ConfirmationDialogComponent, {
          data: {
            text: 'This is a confirmation dialog',
            confirmText: 'Alright',
            declineText: 'D:',
          }
        });
        break;

      case 'user':
        this.matDialog.open(UserDialogComponent, {
          data: {
            title: 'USER DIALOG',
            confirmText: 'Action',
            declineText: 'Cancel',
            password: true,
            admin: true,
          }
        });
        break;
    }
  }

}
