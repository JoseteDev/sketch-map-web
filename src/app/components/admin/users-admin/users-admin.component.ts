import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ConfirmationDialogComponent} from '../../dialogs/confirmation-dialog/confirmation-dialog.component';
import {IUser} from '../../../interfaces/i-user';
import {MatDialog} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Subscription} from 'rxjs';
import {UserDialogComponent} from '../../dialogs/user-dialog/user-dialog.component';
import {UserService} from '../../../services/user.service';
import {StatusCodes} from 'http-status-codes';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.scss']
})
export class UsersAdminComponent implements OnInit, OnDestroy {

  subscriptions: Subscription[] = [];
  dataSource: MatTableDataSource<IUser>;
  displayedColumns: string[] = ['id', 'username', 'email', 'admin', 'actions'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(
    private matDialog: MatDialog,
    private userService: UserService,
    private notifierService: NotifierService
  ) {}


  ngOnInit() {
    this.subscriptions.push(this.userService.userList.subscribe(userList => {
      if (!!userList)
        this.loadTable(userList);
    }));
    this.userService.doGetUsers();
  }


  ngOnDestroy() {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }


  loadTable(userList: IUser[]) {
    this.dataSource = new MatTableDataSource(userList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }


  applyFilter(event: any) {
    this.dataSource.filter = event.target.value.trim().toLowerCase();
    if (this.dataSource.paginator)
      this.dataSource.paginator.firstPage();
  }


  createUser() {
    const dialogRef = this.matDialog.open(UserDialogComponent, {
      data: {
        title: 'CREATE USER',
        confirmText: 'Create',
        declineText: 'Cancel',
        password: true,
        admin: true,
      }
    });
    dialogRef.afterClosed().subscribe(userToCreate => {
      if (userToCreate)
        this.userService.doCreateUser(userToCreate).subscribe(response => {
          if (response.status === StatusCodes.OK) {
            this.userService.doGetUsers();
            this.notifierService.notify('success', 'User created successfully');
          } else {
            this.notifierService.notify('error', 'Error creating user');
          }
        });
    });
  }


  editUser(user: IUser) {
    const dialogRef = this.matDialog.open(UserDialogComponent, {
      data: {
        title: 'EDIT USER',
        confirmText: 'Update',
        declineText: 'Cancel',
        userToUpdate: user,
        admin: true,
      }
    });
    dialogRef.afterClosed().subscribe(userToUpdate => {
      if (userToUpdate)
        this.userService.doUpdateUser(userToUpdate).subscribe(response => {
          if (response.status === StatusCodes.OK) {
            this.userService.doGetUsers();
            this.notifierService.notify('success', 'User updated successfully');
          } else {
            this.notifierService.notify('error', 'Error updating user');
          }
        });
    });
  }


  deleteUser(user: IUser) {
    const dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      data: {
        text: 'Are you sure you want to delete this user? This action cannot be undone',
        confirmText: 'Delete',
        declineText: 'Cancel',
        admin: true,
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result)
        this.userService.doDeleteUser(user).subscribe(response => {
          if (response.status === StatusCodes.OK) {
            this.userService.doGetUsers();
            this.notifierService.notify('success', 'User deleted successfully');
          } else {
            this.notifierService.notify('error', 'Error deleting user');
          }
        });
    });
  }

}
