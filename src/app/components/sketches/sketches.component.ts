import {Component, OnDestroy, OnInit} from '@angular/core';
import {MapService} from '../../services/map.service';
import {SketchService} from '../../services/sketch.service';
import {ISketch} from '../../interfaces/i-sketch';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ConfirmationDialogComponent} from '../dialogs/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {StatusCodes} from 'http-status-codes';
import {NotifierService} from 'angular-notifier';

@Component({
  selector: 'app-sketches',
  templateUrl: './sketches.component.html',
  styleUrls: ['./sketches.component.scss']
})
export class SketchesComponent implements OnInit, OnDestroy {

  readonly zipType = 'application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip';

  subscriptions: Subscription[] = [];
  sketches: ISketch[];
  form: FormGroup;
  loading = false;


  constructor(
    private mapService: MapService,
    private sketchService: SketchService,
    private notifierService: NotifierService,
    private matDialog: MatDialog
  ) {
  }


  ngOnInit() {
    this.subscriptions.push(this.sketchService.sketches.subscribe(sketches => this.sketches = sketches));
    this.sketchService.doGetSketches();

    this.form = new FormGroup({
      file: new FormControl(null, Validators.required),
      name: new FormControl(null, Validators.required)
    });
  }


  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }


  onFileUploaded(event: any) {
    const file = event.target.files[0];
    if (file) {
      this.form.get('file').setValue(file);
      this.form.get('name').setValue(file.name);
    }
  }


  onNameChanged(event: any) {
    this.form.get('name').setValue(event.target.value);
  }


  enableSubmitButton(): boolean {
    return !this.form.get('file').hasError('required') && !this.form.get('name').hasError('required');
  }

  enableClearButton(): boolean {
    return !!this.form.get('file').value || !!this.form.get('name').value;
  }


  clearForm() {
    this.loading = false;
    this.form.reset({file: null, name: null});
  }


  uploadSketch() {
    this.loading = true;

    const file = this.form.get('file').value;
    const name = this.form.get('name').value;
    const center = this.mapService.getCenter();

    const body: FormData = new FormData();
    body.append('name', name ? name : file.name);
    body.append('longitude', `${center.lng}`);
    body.append('latitude', `${center.lat}`);
    body.append('zip_file', file);

    this.sketchService.doCreateSketch(body).subscribe(response => {
      this.loading = false;
      if (response.status === StatusCodes.OK) {
        this.sketchService.doGetSketches();
        this.notifierService.notify('success', 'Sketch created successfully!');
      } else {
        this.notifierService.notify('error', 'Error creating sketch');
      }
    });
  }


  deleteSketch(sketch: ISketch) {
    const dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      data: {
        text: 'Are you sure you want to delete this sketch?',
        confirmText: 'Delete',
        declineText: 'Cancel',
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sketchService.doDeleteSketch(sketch).subscribe(response => {
          if (response.status === StatusCodes.OK) {
            this.sketchService.doGetSketches();
            this.mapService.deleteSketch(sketch);
            this.notifierService.notify('success', 'Sketch deleted successfully!');
          } else {
            this.notifierService.notify('error', 'Error deleting sketch');
          }
        });
      }
    });
  }


  selectSketch(event: any, sketch: ISketch) {
    this.mapService.switchSketchVisibility(sketch, event.checked);
  }


  flyToSketch(sketch: ISketch) {
    this.mapService.flyTo(sketch.longitude, sketch.latitude);
  }

}
