import {AfterViewInit, Component, Input} from '@angular/core';
import ParticlesConfig from '../../../assets/particles.json';

declare var particlesJS;

@Component({
  selector: 'app-particles',
  templateUrl: './particles.component.html',
  styleUrls: ['./particles.component.scss']
})
export class ParticlesComponent implements AfterViewInit {

  @Input() id: string;

  constructor() { }

  ngAfterViewInit(): void {
    particlesJS(this.id, ParticlesConfig);
  }

}
