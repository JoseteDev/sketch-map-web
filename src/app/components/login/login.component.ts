import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {MatDialog} from '@angular/material/dialog';
import {NotifierService} from 'angular-notifier';
import {StatusCodes} from 'http-status-codes';
import {UserService} from '../../services/user.service';
import {UserDialogComponent} from '../dialogs/user-dialog/user-dialog.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private matDialog: MatDialog,
    private authService: AuthService,
    private userService: UserService,
    private notifierService: NotifierService
  ) { }


  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [ Validators.required, Validators.email ]),
      password: new FormControl('', Validators.required)
    });
  }


  hasAnyError(): boolean {
    return  this.loginForm.get('email').hasError('required') ||
      this.loginForm.get('email').hasError('email') ||
      this.loginForm.get('password').hasError('required');
  }


  doSignUp() {
    const dialogRef = this.matDialog.open(UserDialogComponent, {
      data: {
        title: 'CREATE ACCOUNT',
        confirmText: 'Create',
        declineText: 'Cancel',
        password: true,
      }
    });

    dialogRef.afterClosed().subscribe(userToCreate => {
      if (userToCreate) {
        this.userService.doCreateUser(userToCreate).subscribe(response => {
          if (response.status === StatusCodes.OK) {
            this.notifierService.notify('success', 'Account created successfully. Welcome to SketchMap!');
          } else {
            this.notifierService.notify('error', 'Error creating account');
          }
        });
      }
    });
  }


  doForgotPassword() {
    this.notifierService.notify('default', 'Currently in development...');
  }


  doLogin() {
    if (this.hasAnyError())
      return;

    const body = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value
    };
    this.authService.doLogin(body);
  }

}
