import {Component, Inject} from '@angular/core';
import {IUser} from '../../../interfaces/i-user';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EqualsDirective} from '../../../directives/equals.directive';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.scss']
})
export class UserDialogComponent {

  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loadForm();
  }


  loadForm() {
    this.form = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.pattern('(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,30}'), EqualsDirective.equals]),
      confirmPassword: new FormControl('', [Validators.required, EqualsDirective.equals]),
      admin: new FormControl('', Validators.required),
    });

    if (this.data.userToUpdate)
      this.form.patchValue(this.data.userToUpdate);
  }


  hasAnyError(): boolean {
    return this.form.get('username').hasError('required') ||
      this.form.get('email').hasError('required') ||
      this.form.get('email').hasError('email') ||
      (this.showPasswordFields() && (
        this.form.get('password').hasError('required') ||
        this.form.get('password').hasError('pattern') ||
        this.form.get('confirmPassword').hasError('required') ||
        this.form.get('confirmPassword').hasError('equals'))
      ) ||
      (this.showAdminFields() && this.form.get('admin').hasError('required'));
  }


  onConfirmButton() {
    const userToSave = {
      username: this.form.get('username').value,
      email: this.form.get('email').value,

      // Only for update
      id: this.data.userToUpdate ? this.data.userToUpdate.id : undefined,

      // Only for creation
      password: this.showPasswordFields() ? this.form.get('password').value : undefined,

      // Only for admins
      admin: this.showAdminFields() ? this.form.get('admin').value : undefined
    } as IUser;

    this.dialogRef.close(userToSave);
  }


  onDeclineButton() {
    this.dialogRef.close();
  }


  showPasswordFields(): boolean {
    return this.data.password;
  }


  showAdminFields(): boolean {
    return this.data.admin;
  }

}
