import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {IConfirmationDialogData} from '../../../interfaces/i-confirmation-dialog-data';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent {

  public dialogRef: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IConfirmationDialogData
  ) {
    this.dialogRef = dialogRef;
  }

  onConfirmButton() {
    this.dialogRef.close(true);
  }

  onDeclineButton() {
    this.dialogRef.close();
  }

}
