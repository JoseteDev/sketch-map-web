import {Component} from '@angular/core';
import {MatRadioChange} from '@angular/material/radio';
import {MapService} from '../../services/map.service';

@Component({
  selector: 'app-layers',
  templateUrl: './layers.component.html',
  styleUrls: ['./layers.component.scss']
})
export class LayersComponent {

  styles = [
    {
      name: 'Dark',
      source: 'mapbox://styles/mapbox/dark-v10'
    },
    {
      name: 'Light',
      source: 'mapbox://styles/mapbox/light-v10'
    },
    {
      name: 'Satellite',
      source: 'mapbox://styles/mapbox/satellite-v9'
    }
  ];


  constructor(
    private mapService: MapService
  ) { }


  switchLayer(event: MatRadioChange) {
    this.mapService.switchStyle(event.value);
  }

}
