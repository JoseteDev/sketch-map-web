import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginPageComponent} from './components/pages/login-page/login-page.component';
import {HomePageComponent} from './components/pages/home-page/home-page.component';
import {ErrorPageComponent} from './components/pages/error-page/error-page.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AdminPageComponent} from './components/pages/admin-page/admin-page.component';

const routes: Routes = [
  { path: '',       component: HomePageComponent,   canActivate: [AuthGuardService] },
  { path: 'error',  component: ErrorPageComponent,  canActivate: [AuthGuardService] },
  { path: 'admin',  component: AdminPageComponent,  canActivate: [AuthGuardService], data: { admin: true } },
  { path: 'login',  component: LoginPageComponent },
  { path: 'home',   pathMatch: 'full', redirectTo: '' },
  { path: '**',     pathMatch: 'full', redirectTo: 'error' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
