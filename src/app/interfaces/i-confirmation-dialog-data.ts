export interface IConfirmationDialogData {
  text: string;
  confirmText: string;
  declineText: string;
}
