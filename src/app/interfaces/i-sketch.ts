export interface ISketch {
  id: number;
  name: string;
  longitude: number;
  latitude: number;
  zip_file: File;
  user_id: number;
}
