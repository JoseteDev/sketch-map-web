import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {NotifierModule, NotifierOptions} from 'angular-notifier';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {AngularMaterialModule} from './modules/angular-material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import NotificationsConfig from '../assets/notifications.json';

import {EqualsDirective} from './directives/equals.directive';

import {AppComponent} from './app.component';
import {HomePageComponent} from './components/pages/home-page/home-page.component';
import {LoginPageComponent} from './components/pages/login-page/login-page.component';
import {ParticlesComponent} from './components/particles/particles.component';
import {LoginComponent} from './components/login/login.component';
import {ErrorPageComponent} from './components/pages/error-page/error-page.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {DevToolsComponent} from './components/admin/dev-tools/dev-tools.component';
import {ConfirmationDialogComponent} from './components/dialogs/confirmation-dialog/confirmation-dialog.component';
import {InterceptorService} from './services/interceptor.service';
import {MapComponent} from './components/map/map.component';
import {LayersComponent} from './components/layers/layers.component';
import {SketchesComponent} from './components/sketches/sketches.component';
import {AdminPageComponent} from './components/pages/admin-page/admin-page.component';
import {UsersAdminComponent} from './components/admin/users-admin/users-admin.component';
import {UserDialogComponent} from './components/dialogs/user-dialog/user-dialog.component';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginPageComponent,
    ParticlesComponent,
    LoginComponent,
    ErrorPageComponent,
    NavbarComponent,
    DevToolsComponent,
    ConfirmationDialogComponent,
    MapComponent,
    LayersComponent,
    SketchesComponent,
    EqualsDirective,
    AdminPageComponent,
    UsersAdminComponent,
    UserDialogComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        AngularMaterialModule,
        FlexModule,
        FlexLayoutModule,
        HttpClientModule,
        NotifierModule.withConfig(NotificationsConfig as NotifierOptions),
        MatSelectModule,
    ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
