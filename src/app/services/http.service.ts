import { Injectable } from '@angular/core';
import {IApiResponse} from '../interfaces/i-api-response';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IHttpOptions} from '../interfaces/i-http-options';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private httpClient: HttpClient
  ) { }


  get(path: string, options?: IHttpOptions): Observable<IApiResponse> {

    return new Observable((subscriber) => {
      this.httpClient.get(path, options).subscribe(
        (response: IApiResponse) => {
          subscriber.next(response);
        },
        (error: HttpErrorResponse) => {
          subscriber.next(error.error as IApiResponse);
        }
      );
    });
  }


  post(path: string, body?: any, options?: IHttpOptions): Observable<IApiResponse> {

    return new Observable((subscriber) => {
      this.httpClient.post(path, body, options).subscribe(
        (response: IApiResponse) => {
          subscriber.next(response);
        },
        (error: HttpErrorResponse) => {
          subscriber.next(error.error as IApiResponse);
        }
      );
    });
  }


  put(path: string, body?: any, options?: IHttpOptions): Observable<IApiResponse> {

    return new Observable((subscriber) => {
      this.httpClient.put(path, body, options).subscribe(
        (response: IApiResponse) => {
          subscriber.next(response);
        },
        (error: HttpErrorResponse) => {
          subscriber.next(error.error as IApiResponse);
        }
      );
    });
  }


  delete(path: string, options?: IHttpOptions): Observable<IApiResponse> {

    return new Observable((subscriber) => {
      this.httpClient.delete(path, options).subscribe(
        (response: IApiResponse) => {
          subscriber.next(response);
        },
        (error: HttpErrorResponse) => {
          subscriber.next(error.error as IApiResponse);
        }
      );
    });
  }

}
