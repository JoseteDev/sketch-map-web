import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {ISketch} from '../interfaces/i-sketch';
import {HttpService} from './http.service';
import {StatusCodes} from 'http-status-codes';
import {NotifierService} from 'angular-notifier';
import {IApiResponse} from '../interfaces/i-api-response';

@Injectable({
  providedIn: 'root'
})
export class SketchService {

  private sketchesSubject = new BehaviorSubject<ISketch[]>([]);
  public sketches = this.sketchesSubject.asObservable();


  constructor(
    private httpService: HttpService,
    private notifierService: NotifierService
  ) { }


  getSketches(): ISketch[] {
    return this.sketchesSubject.getValue();
  }


  setSketches(sketches: ISketch[]) {
    this.sketchesSubject.next(sketches);
  }


  doGetSketches() {
    this.httpService.get('/sketches').subscribe(response => {
      if (response.status === StatusCodes.OK) {
        this.setSketches(response.data.sketch_list);
      } else {
        this.notifierService.notify('error', 'Error getting sketches');
      }
    });
  }


  doCreateSketch(body: FormData): Observable<IApiResponse> {
    return this.httpService.post('/sketches', body);
  }


  doUpdateSketch(sketchToUpdate: ISketch): Observable<IApiResponse> {
    return this.httpService.put(`/sketches/${sketchToUpdate.id}`, sketchToUpdate);
  }


  doDeleteSketch(sketchToDelete: ISketch): Observable<IApiResponse> {
    return this.httpService.delete('/sketches/' + sketchToDelete.id);
  }

}
