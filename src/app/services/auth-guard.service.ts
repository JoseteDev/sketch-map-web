import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {SessionStorageService} from './session-storage.service';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  jwt: string;

  constructor(
    private router: Router,
    private userService: UserService,
    private sessionStorageService: SessionStorageService,
  ) {
    this.loadSession();
  }


  createSession(data: any) {
    this.sessionStorageService.set('jwt', data.jwt);
    this.sessionStorageService.set('user', data.user);
    this.jwt = data.jwt;
    this.userService.setUser(data.user);
  }


  loadSession() {
    this.jwt = this.sessionStorageService.get('jwt');
    this.userService.setUser(this.sessionStorageService.get('user'));
  }


  clearSession() {
    this.sessionStorageService.clear();
    this.jwt = null;
    this.userService.setUser(null);
  }


  canActivate(route: ActivatedRouteSnapshot) {
    if (!this.jwt) {
      console.log('JWT not found');
      this.sessionStorageService.clear(); // Cleaning session just in case
      this.router.navigate(['/login']);
      return false;
    }

    const user = this.userService.getUser();
    if (route.data.admin && user && !user.admin) {
      this.router.navigate(['']);
      return false;
    }

    return true;
  }

}
