import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  constructor() { }


  getAll(): any {
    return {
      jwt: this.get('jwt'),
      user: this.get('user')
    };
  }


  get(name: string): any {
    const item = sessionStorage.getItem(name);
    return item ? JSON.parse(atob(item)) : null;
  }


  set(name: string, item: any) {
    sessionStorage.setItem(name, btoa(JSON.stringify(item)));
  }


  clear() {
    sessionStorage.clear();
  }

}
