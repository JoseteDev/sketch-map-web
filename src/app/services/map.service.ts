import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import * as MapboxGL from 'mapbox-gl';
import {Layer} from 'mapbox-gl';
import {AuthGuardService} from './auth-guard.service';
import {ISketch} from '../interfaces/i-sketch';

declare var THREE;

@Injectable({
  providedIn: 'root'
})
export class MapService {

  mapbox = MapboxGL as typeof MapboxGL;
  map: MapboxGL.Map;


  constructor(
    private authGuardService: AuthGuardService
  ) {
    this.mapbox.accessToken = environment.mapBoxToken;
  }


  buildMap() {
    this.map = new MapboxGL.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/dark-v10',
      zoom: 15,
      center: [-3.83365, 40.33610],
      antialias: true
    });
    this.map.addControl(new MapboxGL.NavigationControl());
    this.map.addControl(new MapboxGL.FullscreenControl());
    this.map.on('load', () => {
      this.map.resize();
    });
  }


  getCenter() {
    return this.map.getCenter();
  }


  switchStyle(style) {
    this.map.setStyle(style.source);
  }


  flyTo(longitude, latitude) {
    this.map.flyTo({
      center: [longitude, latitude],
      zoom: 16,
      essential: true
    });
  }


  switchSketchVisibility(sketch: ISketch, isVisible: boolean) {
    const layer = this.map.getLayer(`sketch-${sketch.id}`);
    if (layer) {
      this.map.setLayoutProperty(layer.id, 'visibility', isVisible ? 'visible' : 'none');
    } else {
      this.addSketch(sketch);
    }
  }


  addSketch(sketch: ISketch) {
    const jwt = this.authGuardService.jwt;

    // Parameters to ensure the model is geo-referenced correctly on the map
    const modelOrigin = {lng: sketch.longitude, lat: sketch.latitude};
    const modelAltitude = 0;
    const modelRotate = [Math.PI / 2, 0, 0];

    const modelAsMercatorCoordinate = MapboxGL.MercatorCoordinate.fromLngLat(modelOrigin, modelAltitude);

    // Transformation parameters to position, rotate and scale the 3D model onto the map
    const modelTransform = {
      translateX: modelAsMercatorCoordinate.x,
      translateY: modelAsMercatorCoordinate.y,
      translateZ: modelAsMercatorCoordinate.z,
      rotateX: modelRotate[0],
      rotateY: modelRotate[1],
      rotateZ: modelRotate[2],

      // Scale transform needs to be in MercatorCoordinates
      scale: modelAsMercatorCoordinate.meterInMercatorCoordinateUnits()
    };

    // Variables to animate the model
    const clock = new THREE.Clock();
    let mixer;

    // Configuration of the custom layer for a 3D model per the CustomLayerInterface
    const customLayer = {
      id: `sketch-${sketch.id}`,
      type: 'custom',
      renderingMode: '3d',
      onAdd(map, gl) {
        this.map = map;
        this.camera = new THREE.Camera();
        this.scene = new THREE.Scene();

        // Create 3 three.js lights to illuminate the model
        const ambientLight = new THREE.AmbientLight(0xffffdd, 0.6);
        this.scene.add(ambientLight);

        const directionalLight1 = new THREE.DirectionalLight();
        directionalLight1.position.set(1000, 8000, 1000).normalize();
        this.scene.add(directionalLight1);

        const directionalLight2 = new THREE.DirectionalLight();
        directionalLight2.position.set(-1000, 8000, 1000).normalize();
        this.scene.add(directionalLight2);

        // Use the three.js GLTF loader to add the 3D model to the three.js scene
        const loader = new THREE.GLTFLoader();
        loader.manager.setURLModifier(url => `${url}?jwt=${jwt}`);

        loader.load(`${environment.apiUrl}/sketches/${sketch.id}/scene.gltf`, model => {
          mixer = new THREE.AnimationMixer(model.scene);
          model.animations.forEach(clip => mixer.clipAction(clip).play());
          this.scene.add(model.scene);
        });

        // Use the Mapbox GL JS map canvas for three.js
        this.renderer = new THREE.WebGLRenderer({
          canvas: map.getCanvas(),
          antialias: true,
          context: gl
        });

        this.renderer.autoClear = false;
      },
      render(gl, matrix) {
        const rotationX = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(1, 0, 0), modelTransform.rotateX);
        const rotationY = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(0, 1, 0), modelTransform.rotateY);
        const rotationZ = new THREE.Matrix4().makeRotationAxis(new THREE.Vector3(0, 0, 1), modelTransform.rotateZ);

        const m = new THREE.Matrix4().fromArray(matrix);
        const l = new THREE.Matrix4()
          .makeTranslation(modelTransform.translateX, modelTransform.translateY, modelTransform.translateZ)
          .scale(new THREE.Vector3(modelTransform.scale, -modelTransform.scale, modelTransform.scale))
          .multiply(rotationX)
          .multiply(rotationY)
          .multiply(rotationZ);

        if (mixer)
          mixer.update(clock.getDelta());

        this.camera.projectionMatrix = m.multiply(l);
        this.renderer.state.reset();
        this.renderer.render(this.scene, this.camera);
        this.map.triggerRepaint();
      }
    } as unknown as Layer;

    this.map.addLayer(customLayer);
    this.map.on('style.load', () => this.map.addLayer(customLayer));
  }


  deleteSketch(sketch: ISketch) {
    this.map.removeLayer(`sketch-${sketch.id}`);
  }

}
