import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpService} from './http.service';
import {StatusCodes} from 'http-status-codes';
import {IApiResponse} from '../interfaces/i-api-response';
import {IUser} from '../interfaces/i-user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSubject = new BehaviorSubject<IUser>(null);
  public user = this.userSubject.asObservable();

  private userListSubject = new BehaviorSubject<IUser[]>([]);
  public userList = this.userListSubject.asObservable();


  constructor(
    private httpService: HttpService
  ) { }


  getUser(): IUser {
    return this.userSubject.getValue();
  }


  setUser(user: IUser) {
    this.userSubject.next(user);
  }


  getUserList(): IUser[] {
    return this.userListSubject.getValue();
  }


  setUserList(userList: IUser[]) {
    this.userListSubject.next(userList);
  }


  doGetUser(userId: number) {
    this.httpService.get(`/users/${userId}`).subscribe(response => {
      if (response.status === StatusCodes.OK)
        this.setUser(response.data.user);
    });
  }


  doGetUsers() {
    this.httpService.get(`/users`).subscribe(response => {
      if (response.status === StatusCodes.OK)
        this.setUserList(response.data.user_list);
    });
  }


  doCreateUser(userToCreate: IUser): Observable<IApiResponse> {
    return this.httpService.post(`/users`, userToCreate);
  }


  doUpdateUser(userToUpdate: IUser): Observable<IApiResponse> {
    return this.httpService.put(`/users/${userToUpdate.id}`, userToUpdate);
  }


  doDeleteUser(userToDelete: IUser): Observable<IApiResponse> {
    return this.httpService.delete(`/users/${userToDelete.id}`);
  }

}
