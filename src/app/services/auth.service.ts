import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {NotifierService} from 'angular-notifier';
import {StatusCodes} from 'http-status-codes';
import {HttpService} from './http.service';
import {AuthGuardService} from './auth-guard.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
    private httpService: HttpService,
    private notifierService: NotifierService,
    private authGuardService: AuthGuardService,
  ) {}


  doSignUp(body: any) {
    this.httpService.post('/auth/signup', body).subscribe(response => {
      if (response.status === StatusCodes.OK) {
        this.notifierService.notify('success', response.message);
      } else {
        this.notifierService.notify('error', response.message);
      }
    });
  }


  doLogin(body: any) {
    this.httpService.post('/auth/login', body).subscribe(response => {
      if (response.status === StatusCodes.OK) {
        this.authGuardService.createSession(response.data);
        this.router.navigate(['']);
      } else {
        this.notifierService.notify('warning', 'Incorrect user or password');
      }
    });
  }


  doLogout() {
    this.httpService.post('/auth/logout', null).subscribe(response => {
      if (response.status !== StatusCodes.OK)
        console.warn('Logout failed failed. Cleaning session forced');

      this.authGuardService.clearSession();
      this.router.navigate(['/login']);
    });
  }

}
