import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthGuardService} from './auth-guard.service';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {IApiResponse} from '../interfaces/i-api-response';
import {StatusCodes} from 'http-status-codes';
import {catchError} from 'rxjs/operators';
import {environment} from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(
    private router: Router,
    private authGuardService: AuthGuardService,
  ) {}


  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<IApiResponse>> {

    request = request.clone({
      url: environment.apiUrl + request.url,
      setHeaders: {
        Authorization: `Bearer ${this.authGuardService.jwt}`
      }
    });

    return next.handle(request).pipe(catchError(
      error => {
        if (error.status === StatusCodes.FORBIDDEN || error.status === StatusCodes.UNAUTHORIZED) {
          console.log('No session found');
          this.authGuardService.clearSession();
          this.router.navigate(['/login']);
        }
        return throwError(error);
      }
    ));
  }

}
