import { Directive } from '@angular/core';
import {FormControl, ValidationErrors, Validators} from '@angular/forms';

@Directive({
  selector: '[appValidators]'
})
export class EqualsDirective implements Validators {

    static equals(formControl: FormControl): ValidationErrors {

    if (!formControl.parent)
      return null;

    const passwordControl = formControl.parent.get('password');
    const confirmPasswordControl = formControl.parent.get('confirmPassword');
    const error = passwordControl.value === confirmPasswordControl.value ? null : { equals: true };

    if (formControl === confirmPasswordControl)
      return error;

    confirmPasswordControl.setErrors(error);
    return null;
  }


  validate(formControl: FormControl) {
    return EqualsDirective.equals(formControl);
  }

}
