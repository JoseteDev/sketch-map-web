export const environment = {
  production: true,
  apiUrl: 'http://localhost:5000',
  mapBoxToken: 'pk.eyJ1Ijoiam9zZXRlZGV2IiwiYSI6ImNrZmF5eGd4ODB0aHEydG81YnM0cW5zYjEifQ.WVoXAJytx27HFOiR-aWUDw'
};
