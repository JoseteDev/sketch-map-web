pipeline {

    agent any

    stages {
        stage('Build') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                }
            }
            steps {
                sh "echo 'Building Web image...'"
                sh "docker build -t sm-web:beta /var/sketch-map/sketch-map-web"
            }
        }
        stage('Test') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/master'
                    }
                }
            }
            steps {
                script {
                    try {
                        sh "docker rm -f sm-web-beta"
                    } catch (Exception e) {
                        sh "echo 'Web container not found in beta environment D:'"
                    }
                }
                sh "echo 'Deploying Beta environment...'"
                sh "docker-compose -f /var/sketch-map/beta.yml up -d --no-recreate"
                sh "echo 'Running Web tests...'"
                sh "docker-compose -f /var/sketch-map/web-test.yml up" // Without -d to get stdout
            }
            post {
                always {
                    sh "docker cp sm-web-test:/usr/src/app/screenshots /var/jenkins_home/workspace/Web"
                    sh "docker cp sm-web-test:/usr/src/app/reports /var/jenkins_home/workspace/Web"
                    junit skipPublishingChecks: true, testResults: 'reports/*.xml'
                    sh "echo 'Removing testing container...'"
                    sh "docker rm -f sm-web-test"
                }
            }
        }
        stage('Deploy') {
            when {
                allOf {
                    expression {
                        env.GIT_BRANCH == 'origin/stable'
                    }
                }
            }
            steps {
                sh "echo 'Deploying Web...'"
                sh "docker build --build-arg ENVIRONMENT=prod -t sm-web /var/sketch-map/sketch-map-web"
                script {
                    try {
                        sh "docker rm -f sm-web"
                    } catch (Exception e) {
                        sh "echo 'Web container not found in production environment D:'"
                    }
                }
                sh "docker-compose -f /var/sketch-map/production.yml up -d --no-recreate"
            }
        }
    }

    post {
        always {
            chuckNorris()
        }
    }
}
